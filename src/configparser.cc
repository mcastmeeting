/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include <fstream>
#include "configparser.hh"
/* delim to use while parsing */
#define DELIM "="

using namespace std;

ConfigParser::ConfigParser(char *filename)
	:m_filename(filename)
{}

ConfigParser::~ConfigParser() {
	delete m_filename;
}

string ConfigParser::get_value(char *name) {

	string line;
	string::size_type pos;
	string setting_value;
	string setting_name;

	/* open an input stream */
	ifstream conf_file(m_filename);
	if (!conf_file) {
		return "";
	}
	/* read values until match found */
	while( getline(conf_file, line) )
	{
		if( line.empty() );
		else
		{
			/* compare setting names */
			pos = line.find_first_of(DELIM);
			setting_name = line.substr(0, pos);
			if ( setting_name.compare(name) == 0) {
				string match;
				match = string(line.substr(pos+1,
						line.length()).data());
				return match;
			}
		}
	}
}

bool ConfigParser::save_value(char *name, char* value) {
	/* open an output file stream */
	ofstream conf_file(m_filename, ios::out);
	conf_file << name << "=" << value << "\n";
}

