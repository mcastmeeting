/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef _CONFIGPARSER_HH
#define _CONFIGPARSER_HH

#include <iostream>

#ifdef  CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

class ConfigParser
{
	public:
		ConfigParser(char *filename);
		~ConfigParser();

		// get and set functions
		void set_config_file_name(char *filename);
		bool save_value(char *name, char* value);
		std::string get_value(char *name);

	protected:
		char* m_filename;
};
#endif

