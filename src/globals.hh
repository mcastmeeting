/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef _GLOBALS_HH
#define _GLOBALS_HH

#include "widgethandler.hh"

// widget handler
// which implements all widget logic
static WidgetHandler *widgethandler;

// defs for nick handling
#define NICK_ERROR	0
#define NICK_NAME	1
#define NICK_NEW	2
#define NICK_STATUS 3

// message codes
#define MSG_CHAT	0
#define MSG_MBR		1
#define MSG_SYS		2
#define MSG_LOG		3

#endif

