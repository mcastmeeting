/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include "config.h"

#include "globals.hh"
#include "mainwindow.hh"
#include "rtphandler.hh"
#include "propsdialog.hh"
#include "aboutdialog.hh"

mainwindow::mainwindow() {
	/* make window resizable */
	set_resizable(true);

	/* create rtp-handler */
	rtphandler = new mcastRTPHandler;
	/* create global widget-handler */
	widgethandler = new WidgetHandler(this);

	/*
	 * set background color of chat- and log-
	 * widget to black
	 */
	Gdk::Color colBlack;
	colBlack.set_rgb_p (0, 0, 0);
	Gdk::Color colWhite;
	colWhite.set_rgb_p (1, 1, 1);
	textview_chat->modify_base(Gtk::STATE_NORMAL, colBlack);
	textview_chat->modify_text(Gtk::STATE_NORMAL, colWhite);

	textview_log->modify_base(Gtk::STATE_NORMAL, colBlack);
	textview_log->modify_text(Gtk::STATE_NORMAL, colWhite);

	/* add the completion to the command entry */
	Glib::RefPtr<Gtk::EntryCompletion> completion =
		Gtk::EntryCompletion::create();
	entry_command->set_completion(completion);

	/* create and fill the completion's filter model */
	refCompletionModel = Gtk::ListStore::create(m_Entries);
	completion->set_model(refCompletionModel);

	/* display text in the entry when a match is found */
	completion->set_text_column(m_Entries.m_col_entry);

	m_CompletionActions[0] = "/nick";
	m_CompletionActions[1] = "/join";
	m_CompletionActions[2] = "/leave";

	for(type_actions_map::iterator iter = m_CompletionActions.begin();
			iter != m_CompletionActions.end(); ++iter)
	{
		int position = iter->first;
		Glib::ustring title = iter->second;
		completion->insert_action_text(title, position);
	}
	completion->signal_action_activated().connect( sigc::mem_fun(*this,
				&mainwindow::on_completion_action_activated) );
}

mainwindow::~mainwindow() {
	/* clean up */
	delete widgethandler;
	delete rtphandler;
}

void mainwindow::on_item_quit_activate() {
	/* if member -> leave group and exit */
	if (rtphandler->is_member())
		rtphandler->leave_group();
	hide();
}

void mainwindow::on_item_about_activate() {
	/* show the about dialog */
	aboutdialog *aboutdialog = new class aboutdialog();
}

void mainwindow::on_toolbutton_about_clicked() {
	/* show the about dialog */
	aboutdialog *aboutdialog = new class aboutdialog();
}

void mainwindow::on_item_preferences_activate() {
	/* show properties dialog */
	propsdialog *propsdialog = new class propsdialog();
}

void mainwindow::on_toolbutton_prefs_clicked() {
	/* show properties dialog */
	propsdialog *propsdialog = new class propsdialog();
}

void mainwindow::on_toolbutton_quit_clicked() {
	/* if member -> leave group and exit */
	if (rtphandler->is_member())
		rtphandler->leave_group();
	hide();
}

void mainwindow::on_completion_action_activated(int index) {
  type_actions_map::iterator iter = m_CompletionActions.find(index);
  if(iter != m_CompletionActions.end())
  {
    Glib::ustring title = iter->second;
	entry_command->set_text(title);
    //std::cout << "Action selected: " << title << std::endl;
  }
}

void mainwindow::on_entry_command_activate() {
	Glib::ustring c = entry_command->get_text();

	entry_command->set_text("");
	if (c.length() == 0)
		return;

	/* XXX: move this into a command handler class
	 * check if the string entered is a command
	 * commands start with '/' (47 ascii)
	 */
	if (c.at(0) == 47) {
		int sep = c.find_first_of(' ', 0);
		if (sep == -1) {
			widgethandler->append_log("[!] Command '" +
					c.substr(1, c.length()).lowercase() +
					"' needs an argument");
			return;
		}

		Glib::ustring command = c.substr(1, sep-1).lowercase();
		/* we need at least the group */
		Glib::ustring arg1 = c.substr(sep+1, c.length());

		/* atm we support two commands: /join and /leave */
		if (command.compare("join") == 0) {
			/* atm we allow only one group */
			if (rtphandler->is_member()) {
				widgethandler->append_chat(MSG_SYS,
						"[!] You are already a member of a mcast group");
				return;
			}
			widgethandler->append_chat(MSG_SYS, "[i] Trying to join group : '" + arg1 + "'");
			if (rtphandler->join_group(arg1))
				widgethandler->append_chat(MSG_SYS, "[i] Successfully joined group : '" +
						arg1 + "'\n--> Waiting for sync sources...");
			else
				widgethandler->append_chat(MSG_SYS, "[!] Could not join '" + arg1
						+ "'. Not a valid mcast address");
		} else if (command.compare("leave") == 0) {
			if (!rtphandler->is_member()) {
				widgethandler->append_chat(MSG_SYS,
						"[!] You have to join first before you can leave a group");
				return;
			}
			rtphandler->leave_group();
			widgethandler->clear_nicks();
			widgethandler->append_chat(MSG_SYS, "[i] Left multicast group...");
		} else if (command.compare("nick") == 0) {
			rtphandler->set_nickname(arg1);
			widgethandler->append_chat(MSG_SYS,
					"[*] Changed nickname to '" + arg1 + "'");
		} else {
			widgethandler->append_chat(MSG_SYS,
					"[!] Unknown command: " + command);
		}

	} else {
		/* a message has been typed */
		if (rtphandler->is_member()) {
			rtphandler->write_message(c);
			/* append the message to the entries */
			Gtk::TreeModel::Row row = *(refCompletionModel->append());
			row[m_Entries.m_col_entry] = c;
		}
	}

}

void mainwindow::on_online1_activate()
{
	rtphandler->set_status("online");
}

void mainwindow::on_offline1_activate()
{
	rtphandler->set_status("offline");
}

void mainwindow::on_busy1_activate()
{
	rtphandler->set_status("busy");
}

void mainwindow::on_away1_activate()
{
	rtphandler->set_status("away");
}

