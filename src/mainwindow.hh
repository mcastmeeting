/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef _MAINWINDOW_HH
#  include "mainwindow_glade.hh"
#  define _MAINWINDOW_HH

#include "rtphandler.hh"

class mainwindow : public mainwindow_glade
{
	public:
		mainwindow();
		~mainwindow();

	protected:
		// completion stuff
		class EntryColumns : public Gtk::TreeModel::ColumnRecord
		{
			public:
				EntryColumns()
				{ add(m_col_entry); }
				Gtk::TreeModelColumn<Glib::ustring> m_col_entry;
		};
		EntryColumns m_Entries;
		Glib::RefPtr<Gtk::ListStore> refCompletionModel;
		typedef std::map<int, Glib::ustring> type_actions_map;
		type_actions_map m_CompletionActions;

		// event handlers
		virtual void on_completion_action_activated(int index);
		void on_item_quit_activate();
		void on_item_preferences_activate();
		void on_item_about_activate();
		void on_toolbutton_quit_clicked();
        void on_toolbutton_prefs_clicked();
        void on_toolbutton_about_clicked();
		void on_entry_command_activate();

		void on_online1_activate();
        void on_offline1_activate();
        void on_busy1_activate();
        void on_away1_activate();
		// associated rtphandler
		mcastRTPHandler *rtphandler;

};
#endif
