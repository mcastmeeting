/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include "config.h"
#include "propsdialog.hh"

propsdialog::propsdialog() {
	std::string nickname;
	/* init the parser */
	m_parser = new ConfigParser(".mm.settings");
	/* read the values */
	nickname = m_parser->get_value("NICKNAME");
	if ( !nickname.empty() )
		entry_nickname->set_text(nickname);
}

propsdialog::~propsdialog() {
	delete m_parser;
}

void propsdialog::on_cancelbutton_clicked() {
	/* we don't save anything */
	hide();
}

void propsdialog::on_okbutton_clicked() {
	Glib::ustring nick(entry_nickname->get_text());
	if (nick.length() == 0)
		nick = "default";
	/* serialize settings */
	m_parser->save_value("NICKNAME",
			(char *)nick.data());

	/* close dialog */
	hide();
}

