/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef _PROPSDIALOG_HH
#  include "propsdialog_glade.hh"
#  define _PROPSDIALOG_HH

#include "configparser.hh"

class propsdialog : public propsdialog_glade
{
	public:
		propsdialog();
		~propsdialog();
	protected:
		// signal handlers
		void on_cancelbutton_clicked();
		void on_okbutton_clicked();

		// config parser object
		ConfigParser *m_parser;
};
#endif
