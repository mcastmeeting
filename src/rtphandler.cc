/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include <cstdio>
#include <ctime>
#include <ccrtp/rtp.h>

#include "globals.hh"
#include "rtphandler.hh"
#include "configparser.hh"

#ifdef  CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

/* base ports */
const int BASE_PORT = 33634;

mcastRTPHandler::mcastRTPHandler()
	:member(false)
{
	/* init the handler
	 * -> this causes problems sometimes
	 *  XXX: find another way to init
	 *  status and nick
	 */
	//init();
}

bool mcastRTPHandler::init() {
	/* get default app */
	RTPApplication &app = defaultApplication();
	/* set default nick */
	ConfigParser parser(".mm.settings");
	Glib::ustring nickname;
	nickname = parser.get_value("NICKNAME");
	//app.setSDESItem(SDESItemTypeNOTE, "online");
	if ( !nickname.empty() ) {
		app.setSDESItem(SDESItemTypeNAME, nickname);
	} else
		app.setSDESItem(SDESItemTypeNAME, "default");
}

bool mcastRTPHandler::join_group(Glib::ustring group_addr) {
	InetMcastAddress ima;
	try {
		ima = InetMcastAddress(group_addr.data());
	} catch (...) {	}

	if ( ima.isInetAddress() ) {
		/* start the transceiver */
		widgethandler->append_log("Starting transceiver for group: "
				+ group_addr);
		m_transceiver = new mcastRTPTransceiver(ima);
		m_transceiver->start();
		member = true;
		return true;
	} else {
		/* multicast address is not valid */
		widgethandler->append_log("Not a valid multicast address: "
				+ group_addr);
		return false;
	}
}

void mcastRTPHandler::leave_group() {
	m_transceiver->stop();
	member = false;
	delete m_transceiver;
}

bool mcastRTPHandler::write_message(Glib::ustring message) {
	/*
	 * simply calls the send_message() function
	 * of it's associated transceiver
	 */
	m_transceiver->send_message(message);
}

bool mcastRTPHandler::is_member() {
	return member;
}

bool mcastRTPHandler::set_nickname(Glib::ustring nick) {
	/*
	 * set the nick
	 * this fails if dns resolving is not
	 * correctly working
	 */
	RTPApplication &app = defaultApplication();
	app.setSDESItem(SDESItemTypeNAME, nick);
}

bool mcastRTPHandler::set_status(Glib::ustring status) {
	/*
	 * set our status
	 */
	RTPApplication &app = defaultApplication();
	app.setSDESItem(SDESItemTypeNOTE, status);
}

mcastRTPTransceiver::mcastRTPTransceiver(InetMcastAddress group)
	:running(false)
{
	widgethandler->append_log("mcast Transceiver created...");
	/* associate mcast address */
	group_ip = group;

	/* create the session */
	socket = new mcastRTPSession(group_ip,BASE_PORT);
	socket->startRunning();
	ssrc = socket->getLocalSSRC();
}

mcastRTPTransceiver::~mcastRTPTransceiver() {
	/* indicate transceiver destruction in log */
	char buffer[100];
	snprintf(buffer, sizeof(buffer), "Destroying transeiver -ID: %x",
			(int)ssrc);
	widgethandler->append_log(buffer);
	terminate();
	delete socket;
}


void mcastRTPTransceiver::run(void) {
	/* indicate that we are running */
	running = true;

	/* Set up connection */
	socket->setSchedulingTimeout(20000);
	socket->setExpireTimeout(3000000);
	//socket->UDPTransmit::setTypeOfService(SOCKET_IPTOS_LOWDELAY);
	if( !socket->addDestination(group_ip,BASE_PORT) ) {
		char line[30];
		snprintf(line, sizeof(line), "(%x): could not connect to port %d",
				(int)ssrc, BASE_PORT);
		widgethandler->append_log(line);
	}
	char line[65];
	snprintf(line, sizeof(line), "(%x): %s is waiting for messages on port %d...",
			(int)ssrc, group_ip.getHostname(), BASE_PORT);
	widgethandler->append_log(line);
	/* set a static playload for chat messages */
	socket->setPayloadFormat(StaticPayloadFormat(sptMP2T));
	socket->startRunning();

	snprintf(line, sizeof(line), "(%x): queue is %sactive", (int)ssrc,
			( socket->isActive() ? "" : "in"));
	/* This is the main loop, where packets are received */
	while ( running ){

		/* Wait for an RTP packet */
		const AppDataUnit *adu = NULL;
		while ( NULL == adu ) {
			Thread::sleep(10);
			adu = socket->getData(socket->getFirstTimestamp());
		}
		Participant *p = NULL;
		Glib::ustring nick;
		try {
			p = adu->getSource().getParticipant();
		} catch ( ... ) {}
		if ( p == NULL )
			nick = "undefined";
		else
			nick = p->getSDESItem(SDESItemTypeNAME);
		/* display the chat message */
		Glib::ustring msg = Glib::ustring((char*)adu->getData());
		widgethandler->append_chat(MSG_CHAT, "<" + nick + "> : " + msg);
		delete adu;
	}
	widgethandler->append_log("Transceiver thread stoped");
}

bool mcastRTPTransceiver::send_message(Glib::ustring msg) {
	uint32 timestamp = 0;
	time_t sending_time = time(NULL);

	/* get timestamp */
	timestamp = socket->getCurrentTimestamp();
	/* write message to socket */
	unsigned char *message;
	message = (unsigned char*)msg.data();
	socket->putData(timestamp, message,
			strlen((char *)message)+1);

	return true;
}

void mcastRTPTransceiver::stop() {
	/* this stops the thread */
	running = false;
}

