/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef _RTPHANDLER_HH
#define _RTPHANDLER_HH

#include <ccrtp/rtp.h>
#include <cstdio>
#include <ctime>
#include <gtkmm.h>

#include "rtpsession.hh"
#include "treeview_nicks.hh"

#ifdef  CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

class mcastRTPTransceiver;

class mcastRTPHandler
{
	public:
		mcastRTPHandler();

		// member handling
		bool is_member();
		bool set_nickname(Glib::ustring nick);
		bool set_status(Glib::ustring status);
		// group join and leave func
		bool join_group(Glib::ustring group_addr);
		void leave_group();
		// write a message
		bool write_message(Glib::ustring message);

	protected:
		// transceiver thread
		mcastRTPTransceiver *m_transceiver;

		// indicates whether we are a member
		bool member;

	private:
		// used to init the handler
		bool init( void );
};

class mcastRTPTransceiver : public Thread, public TimerPort
{
	private:
		// socket to transmit
		mcastRTPSession *socket;
		// mcast network address
		InetMcastAddress group_ip;
		// identifier of this sender
		uint32 ssrc;

	public:
		mcastRTPTransceiver(InetMcastAddress group);
		~mcastRTPTransceiver();
		void run( void );

		// send message func
		bool send_message(Glib::ustring msg);
		// used to stop the thread
		void stop( void );

	protected:
		// status of the thread, used to stop it
		bool running;

};
#endif

