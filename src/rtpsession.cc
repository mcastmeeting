/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include "globals.hh"
#include "rtpsession.hh"

#ifdef  CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

mcastRTPSession::mcastRTPSession(InetMcastAddress& ima, tpport_t port)
	:RTPSession(ima,port)
{
}

void mcastRTPSession::onNewSyncSource(const SyncSource& src)
{
	/* we've got a new sync. source */
	char buffer[50];
	snprintf(buffer, sizeof(buffer),
			"New synchronization source (%x)",
			(int)src.getID());
	/* log it */
	widgethandler->append_log(buffer);
}

void mcastRTPSession::onGotSR(SyncSource& source,
		SendReport& SR, uint8 blocks) {}

void mcastRTPSession::onGotRR(SyncSource& source,
		RecvReport& RR, uint8 blocks) {}

bool mcastRTPSession::onGotSDESChunk(SyncSource& source,
		SDESChunk& chunk, size_t len)
{
	/* we track new nicks and nickname changes here */
	bool result = RTPSession::onGotSDESChunk(source,chunk,len);

	/* insert nick into widget */
	widgethandler->insert_nick(source.getID(),
			source.getParticipant()->getSDESItem(SDESItemTypeNAME),
			inet_ntoa(source.getNetworkAddress().getAddress()),
			source.getControlTransportPort(),
			source.getParticipant()->getSDESItem(SDESItemTypeNOTE));
	return result;
}

void mcastRTPSession::onGotGoodbye(const SyncSource& source,
		const std::string& reason) {
	/* gather information about the leaving member */
	unsigned int SSRC = (unsigned int)source.getID();
	char buffer[200];
	Glib::ustring nick =
		source.getParticipant()->getSDESItem(SDESItemTypeNAME);
	snprintf(buffer, sizeof(buffer),
			"<-- Member '%s' (SSRC '%x') left group (reason: '%s')",
			nick.data(), SSRC, reason.data());
	widgethandler->append_chat(MSG_MBR, buffer);
	/* remove the nick */
	widgethandler->remove_nick(SSRC);
}

