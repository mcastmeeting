/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef _RTPSESSION_HH
#define _RTPSESSION_HH

#include <cstdlib>
#include <ccrtp/rtp.h>

#ifdef  CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

class mcastRTPSession : public RTPSession
{
	public:
		mcastRTPSession(InetMcastAddress& ima, tpport_t port);

	protected:
		// session handlers
		void onNewSyncSource(const SyncSource& src);
		void onGotSR(SyncSource& source, SendReport& SR, uint8 blocks);
		void onGotRR(SyncSource& source, RecvReport& RR, uint8 blocks);
		bool onGotSDESChunk(SyncSource& source, SDESChunk& chunk,
				size_t len);
		void onGotGoodbye(const SyncSource& source,
				const std::string& reason);

};
#endif

