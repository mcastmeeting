/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include "config.h"
#include "globals.hh"
#include "treeview_nicks.hh"

typedef Gtk::TreeModel::Children type_children;

bool treeview_nicks::on_treeview_nicks_button_press_event(GdkEventButton *ev) {
  /*
   * call base class, to allow normal handling,
   * such as allowing the row to be selected by the right-click:
   */
  bool return_value = TreeView::on_button_press_event(ev);

  /* then do our custom stuff */
  if( (ev->type == GDK_BUTTON_PRESS) && (ev->button == 3) )
  {
    m_Menu_Popup.popup(ev->button, ev->time);
  }

  return return_value;
}

void treeview_nicks::on_menu_file_popup_generic() {
	/* a popup item has been selected -> do nothing :) */
	Glib::RefPtr<Gtk::TreeView::Selection> refSelection =
		get_selection();
	if(refSelection) {
		Gtk::TreeModel::iterator iter =
			refSelection->get_selected();
		if(iter)
			int id = (*iter)[m_Columns.m_col_SSRC];
	}
}

int treeview_nicks::insert_nick(unsigned int SSRC, Glib::ustring nick,
		Glib::ustring addr, unsigned int port, Glib::ustring status) {

	Gtk::TreeModel::Row row;
	if (find_nick(SSRC, row)) {
		/* nick exists */
		if (row[m_Columns.m_col_nick] != nick) {
			row[m_Columns.m_col_nick] = nick;
			return NICK_NAME;
		}
		if (row[m_Columns.m_col_status] != status) {
			row[m_Columns.m_col_status] = status;
			/* change icon as well */
			row[m_Columns.icon] = get_status_icon(status);
			return NICK_STATUS;
		}
	} else {
		/* new nick to append */
		Gtk::TreeModel::Row newrow =
			*(m_refTreeModel->append());

		newrow[m_Columns.icon] = get_status_icon(status);
		newrow[m_Columns.m_col_nick] = nick;
		newrow[m_Columns.m_col_SSRC] = SSRC;
		newrow[m_Columns.m_col_addr] = addr;
		newrow[m_Columns.m_col_port] = port;
		newrow[m_Columns.m_col_status] = status;
		return NICK_NEW;
	}
}

bool treeview_nicks::remove_nick(unsigned int SSRC) {
	/* find nick and remove it */
	type_children::iterator iter;
	if (find_nick_iter(SSRC, iter))
		m_refTreeModel->erase(iter);
}

void treeview_nicks::clear_nicks() {
	m_refTreeModel->clear();
}

bool treeview_nicks::find_nick(unsigned int SSRC,
		Gtk::TreeModel::Row &row) {
	/*
	 * search for a nick with given SSRC
	 * return Row
	 */
	type_children children = m_refTreeModel->children();
	for(type_children::iterator iter = children.begin();
			iter != children.end(); ++iter) {
		row = *iter;
		if (row[m_Columns.m_col_SSRC] == SSRC)
			return true;
	}
	return false;
}

bool treeview_nicks::find_nick_iter(unsigned int SSRC,
		Gtk::TreeModel::Children::iterator &iter) {
	/*
	 * search for a nick with given SSRC
	 * return iterator
	 */
	Gtk::TreeModel::Row row;
	type_children children = m_refTreeModel->children();
	for(iter = children.begin(); iter != children.end(); ++iter) {
		row = *iter;
		if (row[m_Columns.m_col_SSRC] == SSRC)
			return true;
	}
	return false;
}

Glib::RefPtr<Gdk::Pixbuf> treeview_nicks::get_status_icon(Glib::ustring status) {
	/* get suitable icon */
	Glib::RefPtr<Gdk::Pixbuf> icon;
	if ( status.compare("online") == 0)
		icon = get_icon(Gtk::StockID("gtk-connect"));
	else if( status.compare("offline") == 0)
		icon = get_icon(Gtk::StockID("gtk-disconnect"));
	else if( status.compare("busy") == 0)
		icon = get_icon(Gtk::StockID("gtk-dialog-error"));
	else if( status.compare("away") == 0)
		icon = get_icon(Gtk::StockID("gtk-quit"));
	else
		icon = get_icon(Gtk::StockID("gtk-connect"));

	return icon;
}

Glib::RefPtr<Gdk::Pixbuf> treeview_nicks::get_icon(Gtk::StockID id) {

	Gtk::IconSize size(Gtk::ICON_SIZE_SMALL_TOOLBAR);
	Gtk::Window *defwid = new Gtk::Window();
	Glib::RefPtr<Gdk::Pixbuf> pixbuf = defwid->render_icon(id, size);
	delete(defwid);
	return pixbuf;
}

