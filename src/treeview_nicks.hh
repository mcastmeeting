/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef _TREEVIEW_NICKS_HH
#  include "treeview_nicks_glade.hh"
#  define _TREEVIEW_NICKS_HH

#include <gtkmm.h>
#include <sigc++/compatibility.h>

class treeview_nicks : public treeview_nicks_glade
{
public:

        treeview_nicks(GlademmData *gmm_data) : treeview_nicks_glade(gmm_data)
        {
			// create the Tree model:
			m_refTreeModel = Gtk::ListStore::create(m_Columns);
			set_model(m_refTreeModel);

			// add the TreeView's view columns:
			append_column("", m_Columns.icon);
			append_column("Nick", m_Columns.m_col_nick);
			append_column("SSRC", m_Columns.m_col_SSRC);
			append_column("Address", m_Columns.m_col_addr);
			append_column("Port", m_Columns.m_col_port);
			append_column("Status", m_Columns.m_col_status);

			// fill popup menu:
			{
				Gtk::Menu::MenuList& menulist = m_Menu_Popup.items();
				menulist.push_back( Gtk::Menu_Helpers::MenuElem("_Send File",
							SigC::slot(*this, &treeview_nicks::on_menu_file_popup_generic) ) );
				menulist.push_back( Gtk::Menu_Helpers::MenuElem("_Open Video",
							SigC::slot(*this, &treeview_nicks::on_menu_file_popup_generic) ) );
			}
			m_Menu_Popup.accelerate(*this);
        }

		// insert a nick into the tree
		int insert_nick(unsigned int SSRC, Glib::ustring nick,
				Glib::ustring addr, unsigned int port, Glib::ustring status);
		// remove a nick from the tree
		bool remove_nick(unsigned int SSRC);
		// clear list of nicks
		void clear_nicks( void );
		// find a specific nick
		bool find_nick(unsigned int SSRC, Gtk::TreeModel::Row &row);
		// find the iterator of a nick
		bool find_nick_iter(unsigned int SSRC,
				Gtk::TreeModel::Children::iterator &iter);

protected:
		// signal handler
		bool on_treeview_nicks_button_press_event(GdkEventButton *ev);

		// signal handler for popup menu items:
		virtual void on_menu_file_popup_generic();

		// tree model columns:
		class NickColumns : public Gtk::TreeModel::ColumnRecord
		{
			public:
				NickColumns()
					{
						add(icon);
						add(m_col_nick);
						add(m_col_SSRC);
						add(m_col_addr);
						add(m_col_port);
						add(m_col_status);
					}
				Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf> > icon;
				Gtk::TreeModelColumn<Glib::ustring> m_col_nick;
				Gtk::TreeModelColumn<unsigned int> m_col_SSRC;
				Gtk::TreeModelColumn<Glib::ustring> m_col_addr;
				Gtk::TreeModelColumn<unsigned int> m_col_port;
				Gtk::TreeModelColumn<Glib::ustring> m_col_status;
		};
		NickColumns m_Columns;

		// the Tree model:
		Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
		Gtk::Menu m_Menu_Popup;

private:
		// icon function
		Glib::RefPtr<Gdk::Pixbuf> get_icon(Gtk::StockID id);
		Glib::RefPtr<Gdk::Pixbuf> get_status_icon(Glib::ustring status);
};
#endif

