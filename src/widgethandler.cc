/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include <gtkmm.h>
#include "globals.hh"
#include "widgethandler.hh"
#include "mainwindow.hh"

#ifdef  CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

/* associated main-window */
mainwindow *m_win;

WidgetHandler::WidgetHandler(Gtk::Window *win)
{
	m_win = (mainwindow *)win;
	/* init the textview buffers */
	Glib::RefPtr<Gtk::TextBuffer> refChatBuffer =
		m_win->textview_log->get_buffer();
	Glib::RefPtr<Gtk::TextBuffer> refLogBuffer =
		m_win->textview_chat->get_buffer();
	refChatBuffer->create_tag("white")->property_foreground() = "white";
	refChatBuffer->create_tag("gray")->property_foreground() = "gray";
	refChatBuffer->create_tag("green")->property_foreground() = "green";

	refLogBuffer->create_tag("white")->property_foreground() = "white";
	refLogBuffer->create_tag("gray")->property_foreground() = "gray";
	refLogBuffer->create_tag("green")->property_foreground() = "green";
}

WidgetHandler::~WidgetHandler() {

}

void WidgetHandler::append_line(unsigned int type, Gtk::TextView *widget,
		Glib::ustring line) {

	Glib::RefPtr<Gtk::TextBuffer> refTextBuffer = widget->get_buffer();
	/* colorful messages */
	Glib::ustring color;
	switch (type) {
		case MSG_CHAT:
			color = "white";
			break;
		case MSG_MBR:
			color = "green";
			break;
		case MSG_SYS:
			color = "gray";
			break;
		case MSG_LOG:
			color = "white";
			break;
		default:
			return;
			break;

	}
	refTextBuffer->insert_with_tag(refTextBuffer->end(), line + "\n", color);
	Gtk::TextBuffer::iterator end = refTextBuffer->end();
	widget->scroll_to_iter(end, 0.0);
}

void WidgetHandler::append_log(Glib::ustring line) {
	time_t receiving_time = time(NULL);
	char tmstring[30];
	strftime(tmstring,30,"%X",localtime(&receiving_time));
	append_line(MSG_LOG, m_win->textview_log,
			Glib::ustring(tmstring) + " : " + line);
}

void WidgetHandler::append_chat(unsigned int type, Glib::ustring line) {
	append_line(type, m_win->textview_chat, line);
}

void WidgetHandler::insert_nick(unsigned int SSRC, Glib::ustring nick,
		Glib::ustring addr, unsigned int port, Glib::ustring status) {

	char buffer[100];
	/* call insert_nick function of the widget */
	switch (m_win->treeview_nicks->insert_nick(SSRC, nick,
				addr, port, status)) {
		case NICK_NEW:
			snprintf(buffer, sizeof(buffer),
					"--> New member '%s' with SSRC '%x' joined group",
					nick.data(), SSRC);
			append_chat(MSG_MBR, buffer);
			break;
		case NICK_NAME:
			snprintf(buffer, sizeof(buffer),
					"[i] Member with SSRC '%x' changed nick to '%s'",
					SSRC, nick.data());
			append_chat(MSG_SYS, buffer);
			break;
		case NICK_STATUS:
			snprintf(buffer, sizeof(buffer),
					"[i] Member '%s' changed his status to '%s'",
					nick.data(), status.data());
			append_chat(MSG_SYS, buffer);
			break;
		default:
			break;
	}
}

void WidgetHandler::remove_nick(unsigned int SSRC) {
	/* remove the nick */
	m_win->treeview_nicks->remove_nick(SSRC);
}

void WidgetHandler::clear_nicks() {
	/* clear nick table */
	m_win->treeview_nicks->clear_nicks();
}

