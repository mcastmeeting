/*
 * mcastMeeting, proof-of-concept RTP-Multicast chat
 * Copyright (c) 2005 Reto Buerki
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef _WIDGETHANDLER_HH
#define _WIDGETHANDLER_HH

#include <gtkmm.h>

#ifdef  CCXX_NAMESPACES
using namespace ost;
using namespace std;
#endif

class WidgetHandler
{
	public:
		WidgetHandler(Gtk::Window *win);
		~WidgetHandler();

		// methods to write to log
		// and chat widgets
		void append_log(Glib::ustring line);
		void append_chat(unsigned int type, Glib::ustring line);

		// methods for nick handling
		void insert_nick(unsigned int SSRC,
				Glib::ustring nick,
				Glib::ustring addr,
				unsigned int port,
				Glib::ustring status);
		void remove_nick(unsigned int SSRC);
		void clear_nicks();

	private:
		void append_line(unsigned int type,
				Gtk::TextView *widget,
				Glib::ustring line);
};
#endif

